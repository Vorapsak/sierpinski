package WorldStuff;

import Core.Inputs;
import javax.media.opengl.GL2;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * A few demo things, to show some simple capabilities of OpenGL along with
 * implementation examples.
 */
public class DemoStuff extends GameObject{
    //simple demo data
    private double centerx;
    private double centery;
    private double centerz;
    private double rotangle;
    private double radius;
    private double dr;
    
    public DemoStuff(){
        //demo initialization
        centerx = 0;
        centery = 0;
        centerz = 0;
        radius = .7;
        rotangle = 0;
        dr = -.01;
    }
    
    @Override
    public void render(GL2 gl, GLUT glut){
        //simple test drawings
        //Square!
        gl.glBegin(GL2.GL_POLYGON);
        gl.glColor3d(1,0,1);
        gl.glVertex3d(-radius,radius,0);
        gl.glColor3d(1,1,1);
        gl.glVertex3d(radius,radius,0);
        gl.glColor3d(0,0,1);
        gl.glVertex3d(radius,-radius,0);
        gl.glColor3d(0,0,0);
        gl.glVertex3d(-radius,-radius,0);
        gl.glEnd();
        //Teapot!
        gl.glPushMatrix();
        gl.glRotated(rotangle, 0, 1, 1);
        gl.glColor3d(.5,.5,.5);
        glut.glutSolidTeapot(radius);
        gl.glPopMatrix();
        //Text!
        gl.glColor3d(1,1,1);
        gl.glRasterPos2d(-radius, -radius);
        glut.glutBitmapString(8, "Strings!");
        gl.glEnd();
    }

    @Override
    public void handleInput(Inputs in) {
        //nothing to do in here
    }

    @Override
    public void update() {
        //demo code
        radius += dr;
        if(Math.abs(radius) > .7) dr*=-1;
        rotangle += 5;
        rotangle %= 360;
    }
}
