package WorldStuff;

import Core.Inputs;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import javax.media.opengl.GL2;

public class GameWorld {
    //framework objects
    private Inputs in;
    private boolean canUnpause = true;

    private enum GameStates {Play, Pause};
    private GameStates gameState;
    private GLUT glut;
    private ArrayList<GameObject> objects;
    
    private String[] fsrc;
    private String[] vsrc;
    
    //keybindings
    private final int pauseKey = KeyEvent.VK_P;
    private final int quitKey = KeyEvent.VK_ESCAPE;
    
    //constructor, initializes everything
    public GameWorld(Inputs inputs){
        //default initializers
        gameState = GameStates.Play;
        in = inputs;
        glut = new GLUT();
        objects = new ArrayList<GameObject>();
        objects.add(new Player());
        //objects.add(new DemoStuff());
        
        //initShaders();
    }
    //Ugly hack to load Shaders
    private void initShaders() {
       /* try {
            String line;
            BufferedReader brv = new BufferedReader(new FileReader("vertexshader.glsl"));
            vsrc = new String[1];
            vsrc[0] = "";
            while ((line=brv.readLine()) != null) {
            vsrc[0] += line + "\n";
            } 
        } catch(Exception e){
            System.err.println("Error loading vertex shader");
        }
        
        try{
            String line;
            BufferedReader brf = new BufferedReader(new FileReader("fragmentshader.glsl"));
            fsrc = new String[1];
            fsrc[0] = "";
            while ((line=brf.readLine()) != null) {
            fsrc[0] += line + "\n";
            }
        } catch(Exception e){
            System.err.println("Error loading fragment shader");
        }*/
    }

    //draw world state, defer actor rendering to actors (player, etc.)
    //the screen has already been cleared, so we just have to worry about drawing.
    public void render(GL2 gl){
        /*int v = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
        int f = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);

        
        gl.glShaderSource(v, 1, vsrc, null);
        gl.glCompileShader(v);

        
        gl.glShaderSource(f, 1, fsrc, null);
        gl.glCompileShader(f);

        //Don't think of this as a "Program", but rather as a "Shader Profile",
        //the active set of Shaders to be used.
        int shaderprogram = gl.glCreateProgram();
        gl.glAttachShader(shaderprogram, v);
        gl.glAttachShader(shaderprogram, f);
        gl.glLinkProgram(shaderprogram);
        gl.glValidateProgram(shaderprogram);

        gl.glUseProgram(shaderprogram);
        *
        */
        
        for (GameObject o : objects){
            o.render(gl, glut);
        }
        
        
    }

    //not much to see here
    public void update() {
        handleInput();
        if(gameState == GameStates.Play){
            for (GameObject o : objects){
                o.update();
            }
        }
    }
    
    //Defers object control response to the objects
    //Handles gamestate changes and quitting
    private void handleInput(){
        //game objects first
        for (GameObject o : objects){
            o.handleInput(in);
        }
        
        //system-wide stuff second
        if(in.isKeyDown(quitKey)){
            System.exit(0);
        }
        
        //for pausing, we don't want to switch back and forth every frame
        //if they hold down the pause button. So we use a bool for eligibility.
        if(in.isKeyDown(pauseKey)){
            if(gameState == GameStates.Play && canUnpause){
                gameState = GameStates.Pause;
                canUnpause = false;
            }
            else if (canUnpause){
                gameState = GameStates.Play;
                canUnpause = false;
            }
        } else{
            canUnpause = true;
        }
    }
}
