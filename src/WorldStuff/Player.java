package WorldStuff;

import Core.Inputs;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.media.opengl.GL2;

class Player extends GameObject{
    private ArrayList<ArrayList<Triangle>> tris;//, newtris;
    int currentlevel;
    int maxlevel;
    boolean canMove;

    public Player() {
        tris = new ArrayList<ArrayList<Triangle>>();
        tris.add(new ArrayList<Triangle>());
        tris.get(0).add(new Triangle(new Point(-1.0,0.0), new Point(0.0,1.0), new Point(1.0,0.0)));
        
        currentlevel = 0;
        maxlevel = 8;
        canMove = true;
        
        for(int i = 0; i <= maxlevel; i++){
            ArrayList<Triangle> newtris = new ArrayList<Triangle>();
            for(Triangle t : tris.get(i)){
                Point p4 = new Point((t.p1.x+t.p2.x)/2, (t.p1.y+t.p2.y)/2);
                Point p5 = new Point((t.p2.x+t.p3.x)/2, (t.p2.y+t.p3.y)/2);
                Point p6 = new Point((t.p3.x+t.p1.x)/2, (t.p3.y+t.p1.y)/2);
                newtris.add(new Triangle(t.p1,p4,p6));
                newtris.add(new Triangle(p4,t.p2,p5));
                newtris.add(new Triangle(p6,p5,t.p3));
            }
            tris.add(newtris);
        }
    }
    
    @Override
    public void handleInput(Inputs in){
        if(in.isKeyDown(KeyEvent.VK_UP) && canMove){
            currentlevel++;
            currentlevel = Math.min(currentlevel, maxlevel);
            canMove = false;
        } 
        if(in.isKeyDown(KeyEvent.VK_DOWN) && canMove){
            currentlevel--;
            currentlevel = Math.max(currentlevel,0);
            canMove = false;
        } 
        if(!(in.isKeyDown(KeyEvent.VK_DOWN) || in.isKeyDown(KeyEvent.VK_UP)))
        {
            canMove = true;
        }
        
    }
    
    @Override
    public void update(){
        //only update every 15 frames
            
    }

    @Override
    public void render(GL2 gl, GLUT glut) {
        double c = 0;
        for(Triangle t : tris.get(currentlevel)){
            gl.glBegin(GL2.GL_TRIANGLES);
            gl.glColor3d(Math.sin(c),Math.cos(c),Math.tan(c));
            gl.glVertex2d(t.p1.x, t.p1.y);
            gl.glVertex2d(t.p2.x, t.p2.y);
            gl.glVertex2d(t.p3.x, t.p3.y);
            gl.glEnd();
            c += .01;
        }
        gl.glColor3d(0,1,0);
        gl.glRasterPos2d(-1,-.98);
        glut.glutBitmapString(8, "Up and Down to iterate");
        gl.glEnd();
        //Random rand = new Random();
        //gl.glRotatef(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
    }
    
}
