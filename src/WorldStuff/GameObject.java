package WorldStuff;

//This class is the superclass for all GameObjects, including Player and Enemy classes

import Core.Inputs;
import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL2;

public abstract class GameObject {
    public abstract void handleInput(Inputs in);
    public abstract void update();
    public abstract void render(GL2 gl, GLUT glut);
}