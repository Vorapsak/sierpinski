package Core;

import com.jogamp.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

//Open everything up, set up the Canvas in a Window and then give control to the subsystems
public class JOGLBase {
    public static void main(String[] args) {
        GLProfile glp = GLProfile.getDefault();
        GLCapabilities caps = new GLCapabilities(glp);
        GLCanvas canvas = new GLCanvas(caps);
        Inputs i = new Inputs();

        Frame frame = new Frame("Sierpinski Triangles");
        frame.setSize(800, 600);
        frame.add(canvas);
        frame.setVisible(true);
        
        // by default, an AWT Frame doesn't do anything when you click
        // the close button; this bit of code will terminate the program when
        // the window is asked to close
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        Inputs inputs = new Inputs();
        Scene scene = new Scene(inputs);
        canvas.addGLEventListener(scene);
        canvas.addKeyListener(inputs);
        canvas.addMouseListener(inputs);
        canvas.requestFocus();

        Animator animator = new Animator(canvas);
        animator.add(canvas);
        animator.start();
    }
}