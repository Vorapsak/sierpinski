package Core;

import WorldStuff.GameWorld;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

//This class contains the GameWorld and makes sure it can interact nicely with the OpenGL systems
public class Scene implements GLEventListener {
    
    private GameWorld gameWorld;
    Inputs inputs;
    
    public Scene(Inputs input){
        super();
        this.inputs = input;
        this.gameWorld = new GameWorld(inputs);
    }
    
    @Override
    public void init(GLAutoDrawable glad) {
         glad.getGL().setSwapInterval(1);
    }

    //we just let Java take care of the garbage collection, unless there's
    //something significant for us to do here.
    @Override
    public void dispose(GLAutoDrawable glad) {
        
    }

    //this is the Update step, not the render step
    @Override
    public void display(GLAutoDrawable glad) {
        gameWorld.update();
        render(glad);
    }

    //if you need to do anything when the screen size changes, here's the place
    @Override
    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
        
    }

    //this, obviously, is the render step
    private void render(GLAutoDrawable glad) {
        //grab our gl object
        GL2 gl = glad.getGL().getGL2();
        //clear the buffer
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        //render the world
        gameWorld.render(gl);
    }
    
}
