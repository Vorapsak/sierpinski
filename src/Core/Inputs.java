package Core;

import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

//Class to interact with the player easily
public class Inputs implements KeyListener, MouseListener, MouseMotionListener {
    private boolean mouseDown;
    private int mousex;
    private int mousey;
    private Map<Integer,Boolean> keys;
    
    //constructor
    public Inputs(){
        mouseDown = false;
        mousex = 0;
        mousey = 0;
        keys = new HashMap<Integer,Boolean>(){};
        
    }
    
    //accessors
    // <editor-fold>
    public int getMouseX(){
        return this.mousex;
    }
    
    public int getMouseY(){
        return this.mousey;
    }
    
    public boolean isKeyDown(int keycode){
        if(keys.containsKey(keycode)) return keys.get(keycode);
        return false;
    }
    
    public boolean isMouseDown(){
        return this.mouseDown;
    }
    
    public Map<Integer,Boolean> getKeysDown(){
        return keys;
    }
    // </editor-fold>
    
    //internals, wrapping up the KeyListener, MouseMotionListener, and MouseListener
    // <editor-fold>
    @Override
    public void keyPressed(KeyEvent ke) {
        keys.put(ke.getKeyCode(), true);
        //System.out.println("Pressed Key: " + ke.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        keys.put(ke.getKeyCode(),false);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
    }

    @Override
    public void mousePressed(MouseEvent me) {
        mouseDown = true;
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        mouseDown = false;
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        mousex = me.getX();
        mousey = me.getY();
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        
    }
    // </editor-fold>
}
